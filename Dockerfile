FROM php:7.2-fpm-alpine

WORKDIR /var/www/html/

RUN docker-php-ext-install pdo pdo_mysql

RUN chown -R www-data:www-data /var/www/html/

RUN chmod -R 755 /var/www/html/